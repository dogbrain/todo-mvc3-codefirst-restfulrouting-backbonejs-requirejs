﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ToDoDemo.Models
{
    public class ToDo
    {
        [Required]
        public int Id { get; set; }
        
        public int Order { get; set; }
        
        public bool Done { get; set; }
        
        [Required]
        [StringLength(128)]
        public string Text { get; set; }
    }
}