﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using ToDoDemo.Models;

namespace ToDoDemo.DAL
{
    public class ToDoDbContext : DbContext
    {
         /// <summary>
        /// Constructor
        /// 
        /// Set the databaseconnection sting as the name to use in web.config in base constructor
        /// </summary>
        public ToDoDbContext()
            : base("DefaultConnection")
        {}

        public DbSet<ToDo> ToDos { get; set; }
    }
}