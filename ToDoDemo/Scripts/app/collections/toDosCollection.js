﻿// Filename: collections/todos
define([
  'underscore',
  'backbone',
  // Pull in the Model module from above
  'models/toDoModel'
], function (_, Backbone, ToDoModel) {
    var ToDoCollection = Backbone.Collection.extend({
        model: ToDoModel,
        // the url of the webservice
        url: function () {
            return "todos" + (this.get('id') ? '?todoId=' + this.get('id') : '')
        },
        // Filter down the list of all todo items that are finished.
        // Generates a list of all the todo's that are not marked done
        done: function () {
            return this.filter(function (todo) {
                return todo.get('Done');
            });
        },
        // Filter down the list to only todo items that are still not finished.
        remaining: function () {
            return this.without.apply(this, this.done());
        },
        // We keep the Todos in sequential order, despite being saved by unordered
        // GUID in the database. This generates the next order number for new items.
        nextOrder: function () {
            if (!this.length) return 1;
            return this.last().get('Order') + 1;
        },

        // Todos are sorted by their original insertion order.
        comparator: function (todo) {
            return todo.get('Order');
        }

    });
    // You don't usually return a collection instantiated
    return ToDoCollection;
});