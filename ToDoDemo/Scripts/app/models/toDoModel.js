﻿define([
  'underscore',
  'backbone'
], function (_, Backbone) {
    var ToDoModel = Backbone.Model.extend({
        // Set the id of the model (the only id that is automaticly loaded is lowercase id)
        idAttribute: "Id",
        defaults: function () {
            return {
                Done: false,
            };
        },
        toggle: function () {
            this.save({ Done: !this.get("Done") });
        }
    });
    // Return the model for the module
    return ToDoModel;
});