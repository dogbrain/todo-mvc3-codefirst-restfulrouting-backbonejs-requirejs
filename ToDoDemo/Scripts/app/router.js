﻿// Filename: router.js
define([
  'jquery',
  'underscore',
  'backbone',
  'views/todos/todoList',
], function ($, _, Backbone, ToDoListView) {
    var AppRouter = Backbone.Router.extend({
        routes: {
            // Setup routes that have dependencies (dependencies are configured further down) //'/users': 'showUsers',
            //root
            '': 'index',
            // Default - catch all
            '*actions': 'defaultAction'
        }
    });

    var initialize = function () {
        var app_router = new AppRouter;

        //When on index route load toDoListView and render it
        app_router.on('route:index', function () {
            // load toDoListView
            var toDoListView = new ToDoListView();
            //render it
            toDoListView.render();
        }),
    

        //default route... 
        app_router.on('defaultAction', function (actions) {
          
            // We have no matching route, lets just log what the URL was
            console.log('No route:', actions);
          
        });
        //When all of your Routers have been created, and all of the routes are set up properly, 
        //call Backbone.history.start() to begin monitoring hashchange events, and dispatching routes.
        Backbone.history.start();
    };
    return {
        initialize: initialize
    };
});