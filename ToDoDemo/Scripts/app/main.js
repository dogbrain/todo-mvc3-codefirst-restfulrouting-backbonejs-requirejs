﻿// Filename: main.js
require.config({
    baseUrl: 'Scripts', //Setup base folder
    paths: {
        //Setup application path
        app: 'app/app',
        router: 'app/router',
        //Setup jquery to reflect a version
        jquery: 'jquery-1.9.1',
        //Setup MVC & templates to have shorter paths
        collections: 'app/collections',
        models: 'app/models',
        templates: 'app/templates',
        views: 'app/views',
    },

    shim: {
        //fix load underscore
        underscore: {
            exports: '_'
        },
        //setup dependencies for backbone
        backbone: {
            deps: ["underscore", "jquery"],
            exports: "Backbone"
        }
    }


});

require(['app', ],
    function (App) {
        // The "app" dependency is passed in as "App"
        App.initialize();
    });

