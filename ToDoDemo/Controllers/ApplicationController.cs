using System;
using System.Web.Mvc;
using RestfulRouting.Format;
using ToDoDemo.DAL;

namespace ToDoDemo.Controllers
{
    public abstract class ApplicationController : Controller
    {

        protected ToDoDbContext db = new ToDoDbContext();

        protected ActionResult RespondTo(Action<FormatCollection> format)
        {
            return new FormatResult(format);
        }
    }
}