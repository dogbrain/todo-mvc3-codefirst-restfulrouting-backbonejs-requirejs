﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ToDoDemo.Controllers
{
    public class HomeController : ApplicationController
    {
        
        public ActionResult Index()
        {
            return View();
        }

    }
}
