﻿using ToDoDemo.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ToDoDemo.Controllers
{
    public class ToDoController : ApplicationController
    {
        //
        // GET: /ToDo/

        /// <summary>
        /// Get all the ToDo's
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return Json(db.ToDos, JsonRequestBehavior.AllowGet);
        }


        /// <summary>
        /// Shows the specified ToDo.
        /// </summary>
        /// <param name="id">The id.</param>
        /// <returns></returns>
        public ActionResult Show(int id)
        {
            ActionResult result = null;
            var toDo = (from x in db.ToDos
                        where x.Id == id
                        select x).FirstOrDefault<ToDo>();
            if (toDo == null)
                result = new HttpNotFoundResult(); //Item not found
            else
                result = Json(toDo, JsonRequestBehavior.AllowGet);
            return result;

        }


        /// <summary>
        /// Creates the specified ToDo.
        /// </summary>
        /// <param name="toDo">To do.</param>
        /// <returns></returns>
        public ActionResult Create(ToDo toDo)
        {
            ToDo saved=db.ToDos.Add(toDo);
            db.SaveChanges();
            return Json(saved);
        }

        /// <summary>
        /// Updates the specified ToDo.
        /// </summary>
        /// <param name="id">The id.</param>
        /// <param name="newToDo">The new to do.</param>
        /// <returns></returns>
        public ActionResult Update(int id, ToDo newToDo)
        {
            ActionResult result = null;
            var existingToDo = (from x in db.ToDos
                        where x.Id == newToDo.Id
                        select x).FirstOrDefault<ToDo>();
            if(existingToDo==null)
                result = new HttpNotFoundResult(); //Item not found
            existingToDo.Order = newToDo.Order;
            existingToDo.Done = newToDo.Done;
            existingToDo.Text = newToDo.Text;
            db.SaveChanges();
            result = Json(existingToDo);
            return result;

        }

        /// <summary>
        /// Destroys the specified ToDo.
        /// </summary>
        /// <param name="id">The id.</param>
        /// <returns></returns>
        public ActionResult Destroy(int id)
        {
            ActionResult result = null;
            ToDo todo = (from x in db.ToDos
                         where x.Id == id
                         select x).FirstOrDefault<ToDo>();

            if (todo != null)
            {
                db.ToDos.Remove(todo);
                db.SaveChanges();
                result = Json(new { success = true }); //200 with message
            }else
                result = new HttpNotFoundResult(); //Item not found

            return result;
        }

    }
}
