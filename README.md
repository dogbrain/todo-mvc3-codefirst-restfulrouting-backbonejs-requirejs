# ToDo RESTFul
This project contains an MVC 3 [ToDo project][1] that can be used as a base for making your own RESTful webb application.
I have made it for me so that I can remember what to do but you are welcome to use it and to comment on it. 
All suggestions for improvements and comments are welcome just pull/fork and push as much as you want.

## Explaination

### Code First
Through entity framework see DAL/ToDoDbContext.se for more info.

The database used is in the App_Data folder and is an MS SQL CE database.

### RestfuRouting
Routes are defined in Routes.cs and not in Global.asax. 

For more information about RestfulRouting please visit their [homepage][3].

### RequireJS
A great tool for importing just the required resources for a page. The configuration files (main.js and app.js) are all under /Scripts/app

For more information about RequireJS please visit their [homepage][4].

### Backbone.js
The RESTful client side. All configuration and models, collections  etc are found under /Scripts/app

#### Important files
(all paths are relative from /Scripts/app)

* router.js 
> Where to load what

* models (folder)
> Here all the models files are placed. Important if the model does not have an id named id in lowercase, then we need to define what property that is an id (see toDoModel.js). 

* collections (folder)
> Here all the collections are placed

* views
> Here all the logic for the views are placed

* templates
> Here we place all the templates that are used by the views.

## Resources

### NuGet
The following dependencys has been imported/installed with nuget 

* RestfulRouting - Install-Package RestfulRouting
* RequireJS - Install-Package RequireJS
* JQuery 1.9.1 - Install-Package jQuery -Version 1.9.1
* Backbone.js - Install-Package Backbone.js (will install underscore.js also)

### Other

The following items have been installed in another way

* text.js - downloaded directly from [https://github.com/requirejs/text][2]

[1]: http://todomvc.com/
[2]: https://github.com/requirejs/text
[3]: http://www.restfulrouting.com/
[4]: http://requirejs.org/
[5]: http://backbonejs.org/